<?php




use Illuminate\Support\Facades\Route;
use phpDocumentor\Reflection\Types\Resource_;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//----------------------------------------------------------------------------------------------------
// Otras
//----------------------------------------------------------------------------------------------------

Auth::routes();

Route::get('/', 'HomeController@index2');


//----------------------------------------------------------------------------------------------------
// Student
//----------------------------------------------------------------------------------------------------

Route::get('/estudiante', function () {
    return view('student.Dashboard');
})->middleware('Mid_Student');;

//----------------------------------------------------------------------------------------------------
// Administrator
//----------------------------------------------------------------------------------------------------



Route::get('/home', 'HomeController@index')->name('home')->middleware('Redirects');

//----------------------------------------------------------------------------------------------------
// Faculty
//----------------------------------------------------------------------------------------------------

Route::resource('user', 'UserController');
Route::resource('administrador', 'AuthorController')->middleware('Mid_Administrator');;

Route::get('facultades','FacultyController@index')->name('facultades')->middleware('Mid_Administrator');;
Route::post('facultades/create','FacultyController@create')->name('faculties.create')->middleware('Mid_Administrator');;
Route::post('facultades/store','FacultyController@store')->name('facultades.store');
Route::post('facultades/delete/{Faculty}','FacultyController@delete')->name('facultades.delete')->middleware('Mid_Administrator');;
Route::get('facultades/edit/{Faculty}','FacultyController@edit')->name('facultades.edit')->middleware('Mid_Administrator');;
Route::put('facultades/update','FacultyController@update')->name('facultades.update')->middleware('Mid_Administrator');;

//----------------------------------------------------------------------------------------------------
// Programs
//----------------------------------------------------------------------------------------------------

Route::get('Programas','ProgramController@index')->name('programs')->middleware('Mid_Administrator');;
Route::get('programas/crear','ProgramController@create')->name('programs.create')->middleware('Mid_Administrator');;
Route::post('programas/store', 'ProgramController@store')->name('programs.store')->middleware('Mid_Administrator');;
Route::delete('programas/delete/{Program}', 'ProgramController@delete')->name('programs.delete')->middleware('Mid_Administrator');;
Route::get('programas/edit/{program}', 'ProgramController@edit')->name('programs.edit')->middleware('Mid_Administrator');;
Route::put('programas/update','ProgramController@update')->name('programs.update')->middleware('Mid_Administrator');;

//----------------------------------------------------------------------------------------------------
// Campus
//----------------------------------------------------------------------------------------------------

Route::get('campus','CampusController@index')->name('campus')->middleware('Mid_Administrator');;
Route::get('campus/crear','CampusController@create')->name('campus.create')->middleware('Mid_Administrator');;
Route::post('campus/store','CampusController@store')->name('campus.store')->middleware('Mid_Administrator');;
Route::delete('campus/delete/{campus}','CampusController@delete')->name('campus.delete')->middleware('Mid_Administrator');;
Route::get('campus/edit/{campus}', 'CampusController@edit')->name('campus.edit')->middleware('Mid_Administrator');;
Route::put('campus/update', 'CampusController@update')->name('campus.update')->middleware('Mid_Administrator');;


