<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class userSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campuses')->insert([
            'cod_campus' => 1,
            'name_campus' => 'prueba',
            'direction' => 'direction',
        ]);

        DB::table('faculties')->insert([
            'cod_faculty' => 1,
            'name_faculty' => 'ingenieria',
            'cod_campus' => 1,
        ]);

        DB::table('users')->insert([
            'identification_card' => 1,
            'email' => 'ylinares@unbosque.edu.co',
            'type_user' => 2,
            'password' => Hash::make('1234'),
            'cod_faculty' => 1,
        ]);

        DB::table('users')->insert([
            'identification_card' => 2,
            'email' => 'fc@gmail.com',
            'type_user' => 1,
            'password' => Hash::make('1234'),
            'cod_faculty' => 1,
        ]);

        DB::table('users')->insert([
            'identification_card' => 3,
            'email' => 'fc@gmail.com',
            'type_user' => 2,
            'password' => Hash::make('1234'),
            'cod_faculty' => 1,
        ]);

        DB::table('programs')->insert([
            'cod_program' => 1,
            'name_program' => 'Ingeniería de sistemas',
            'cod_faculty' => 1,
            'image'=> 'sistemas.jpg'
        ]);

        DB::table('programs')->insert([
            'cod_program' => 2,
            'name_program' => 'Ingeniería electronica',
            'cod_faculty' => 1,
            'image'=> 'electronica.jpg'
        ]);

        DB::table('program__users')->insert([
            'id' => 1,
            'cod_program' => 2,
            'identification_card' => 2
        ]);

        DB::table('program__users')->insert([
            'id' => 2,
            'cod_program' => 2,
            'identification_card' => 3
        ]);
    }
}
