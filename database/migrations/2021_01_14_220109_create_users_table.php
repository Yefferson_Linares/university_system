<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('identification_card');
            $table->string('name')->nullable($value = true);
            $table->string('direction')->nullable($value = true);
            $table->string('email')->nullable($value = true);
            $table->string('telephone')->nullable($value = true);
            $table->string('city_​​of_residence')->nullable($value = true);
            $table->string('city_​​of_origin')->nullable($value = true);
            $table->string('nationality')->nullable($value = true);
            $table->integer('type_user')->nullable($value = true);
            $table->string('program')->nullable($value = true);
            $table->string('double_degree')->nullable($value = true);
            $table->string('password')->nullable($value = true);
            $table->integer('cod_faculty')->unsigned();
            $table->foreign('cod_faculty')->references('cod_faculty')->on('faculties');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
