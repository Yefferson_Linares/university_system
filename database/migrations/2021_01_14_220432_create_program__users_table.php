<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program__users', function (Blueprint $table) {
            $table->id();

            $table->integer('cod_program')->unsigned();
            $table->foreign('cod_program')->references('cod_program')->on('programs');

            $table->integer('identification_card')->unsigned();
            $table->foreign('identification_card')->references('identification_card')->on('users');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program__users');
    }
}
