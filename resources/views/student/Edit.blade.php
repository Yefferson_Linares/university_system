<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <form class="user" action="{{ route('user.update',$user->identification_card) }}" method="post" >
    @method('PUT')
    @csrf
        <table class="table table-dark">

        <thead>
          <tr>
           <th> Datos</th>
           
            <th> Edicion</th>
          
            
            
          </tr>
        </thead>
       
        <tbody>
       
           
          <tr>
            <th scope="col">nombre </th>
            <td>
            
                <div class="form-group row">
                    <input  value="{{$user->name }}"  id= "name" name="name" class="form-control" >
                    </div>
            </td>
            
          </tr>
         
          <tr>
           
            <th scope="col">direccion</th>
         
           
            <td>
                <div class="form-group row">
                    <input  value="{{$user->direction }}"  id= "direction" name="direction" class="form-control" >
                    </div>
            
            </td>
        
          </tr>
          <tr>
          
            <th scope="col">email</th>
            
         
            <td>
              
                <div class="form-group row">
                <input  value="{{$user->email}}"  id= "email" name="email" class="form-control" >
                </div>
            
            </td>
            
        
          </tr>

          <tr>
      

            <th scope="col">telefono </th>
           
            <td>
                
                <div class="form-group row">
                    <input  value="{{$user->telphone}} "  id= "telphone" name="telphone" class="form-control" >
                </div>

            </td>
            
        
          </tr>

          <tr>

           
            <th scope="col">ciudad_de_redecidencia</th>
       
    
            <td>
            
                <div class="form-group row">
                    <input  value="{{$user->city_​​of_residence}}"  id= "telphone" name="telphone" class="form-control" >
                </div>
            
            </td>
            
        
          </tr>

          <tr>

            <th scope="col">ciudad_de_origen</th>
           
           
            <td>
            
                <div class="form-group row">
                    <input  value="{{$user->city_​​of_origin}}"  id= "city_​​of_origin" name="city_​​of_origin" class="form-control" >
                </div>
            
            </td>
            
        
          </tr>

          
          <tr>

         
            <th scope="col">nacionalidad</th>
          
            <td>

            <div class="form-group row">
                <input  value="{{$user->nationality}}"  id= "city_​​of_origin" name="city_​​of_origin" class="form-control" >
            </div>

        </td>
          </tr>


          <tr>
            
            <th scope="col">facultad</th>
          
            <td>{{$user->faculty}}</td>
          </tr>


          <tr>
          
            <td>

                <button type="submit" class="btn btn-dark btn-user btn-block">
                  Editar
               </button>
            
            
            </td>
          </tr>



        </tbody>
      </table>


    </form> 

      

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>