@extends('layouts.app')

@section('content')
<form action="{{ route('facultades.update') }}" method="post">
    @csrf
    @method('PUT')
    <input type="hidden" name="cod_faculty" value="{{ $faculty->cod_faculty }}">
    <label for="name_faculty">Nombre de la facultad</label>
    <input type="text" name="name_faculty" id="name_faculty" value="{{ $faculty->name_faculty }}"><br><br>
    <label for="cod_campus">Campus</label>
    <select name="cod_campus" id="cod_campus">
        @foreach ($campuses as $row)
        <option value="{{ $row->cod_campus }}">{{ $row->name_campus }}</option>
        @endforeach
    </select><br><br>
    <button class="btn btn-primary">Guardar cambios</button>
</form>
@endsection
