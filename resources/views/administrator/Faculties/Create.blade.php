@extends('layouts.app')

@section('content')
    <form action="{{ route('facultades.store') }}" method="post">
        @csrf
        <label for="name_faculty">Nombre de la facultad</label>
        <input type="text" name="name_faculty" id="name_faculty"><br>
        <label for="cod_campus">Campus</label>
        <select name="cod_campus" id="cod_campus">
            @foreach ($campuses as $row)
            <option value="{{ $row->cod_campus }}">{{ $row->name_campus }}</option>
            @endforeach
        </select><br>
        <button class="btn btn-primary">Crear</button>
    </form>
@endsection
