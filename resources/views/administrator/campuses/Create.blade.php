@extends('layouts.app')

@section('content')
    <form action="{{ route('campus.store') }}" method="post">
        @csrf
        <label for="name_campus">Nombre</label>
        <input type="text" name="name_campus"><br>
        <label for="direction">Dirección</label>
        <input type="text" name="direction"><br>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
@endsection
