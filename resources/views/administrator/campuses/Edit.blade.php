@extends('layouts.app')

@section('content')
    <form action="{{ route('campus.update') }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="cod_campus" value="{{ $campus->cod_campus }}">
        <label for="name_campus">Nombre</label>
        <input type="text" name="name_campus" value="{{ $campus->name_campus }}"><br>
        <label for="direction">Dirección</label>
        <input type="text" name="direction" value="{{ $campus->direction }}"><br>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
@endsection
