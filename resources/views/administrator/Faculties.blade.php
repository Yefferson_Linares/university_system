@extends('layouts.app')

@section('content')

<div class="container">
    <form action="{{ route('faculties.create') }}" method="GET">
        @csrf
        <button type="submit" class="btn btn-primary">Crear una nueva facultad</button>
    </form>
</div>
<br>
<br>
<div class="container">

    <table class="table">
        <th>Código de facultad</th>
        <th>Nombre de facultad</th>
        <th>Campus</th>
        <th>Opciones</th>

        @foreach ($faculties as $row)
        <tr>
            <td>{{ $row->cod_faculty }}</td>
            <td>{{ $row->name_faculty }}</td>
            <td>{{ $row->cod_campus }}</td>
            <td>
                <form action="{{ route('facultades.delete',$row) }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
                <a href="{{ route('facultades.edit',$row) }}" class="btn btn-warning">Editar</a>
            </td>
        </tr>
        @endforeach

    </table>
</div>

@endsection
