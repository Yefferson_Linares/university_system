@extends('layouts.app')

@section('content')

<form action="{{ route('campus.create') }}" method="GET">
    @csrf
    <button type="submit" class="btn btn-primary">Crear nuevo campus</button>
</form>

<table class="table">
    <th>Código de Campus</th>
    <th>Nombre de Campus</th>
    <th>Dirección</th>
    <th>Opciones</th>

    @foreach ($campuses as $row)
    <tr>
        <td>{{ $row->cod_campus }}</td>
        <td>{{ $row->name_campus }}</td>
        <td>{{ $row->direction }}</td>
        <td>
            <form action="{{ route('campus.delete',$row) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Eliminar</button>
            </form>
            <a href="{{ route('campus.edit',$row) }}" class="btn btn-warning">Editar</a>
        </td>
    </tr>
    @endforeach
</table>

@endsection
