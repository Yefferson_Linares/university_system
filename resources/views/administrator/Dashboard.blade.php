<html>
  <head>


   
    <script type="text/javascript">
        const dfeg1 = {!! json_encode($consulta) !!};



             var productos = [];
            var valores = [];
           
            for (let index = 0; index < dfeg1.length; index++){
        productos.push(dfeg1[index].name_program);
        valores.push(dfeg1[index].cantidad);
    }

    </script>

    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        for(i = 0; i < productos.length; i++)
        data.addRow([ productos[i], valores[i]]);
    

        // Set chart options
        var options = {'title':'Estudiantes por programa',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

    
<div class="container">

    <form action="{{ route('logout') }}" method="post">
        @csrf
        <button type="submit">logout</button>
    </form>



</div>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
 

   





    <nav>
        <a href="{{ route('facultades') }}"><li>Facultades</li></a>
        <a href="{{ route('programs') }}"><li>Programas</li></a>
        <a href="{{ route('campus') }}"><li>Campus</li></a>
    </nav>

<h1>Administrador</h1>
<div id="chart_div"></div>
  </body>
</html>
