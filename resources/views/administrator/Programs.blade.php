@extends('layouts.app')

@section('content')

<form action="{{ route('programs.create') }}" method="GET">
    @csrf
    <button type="submit" class="btn btn-primary">Crear nuevo programa</button>
</form>

<br><br>

<table class="table">
    <th>Código de Programa</th>
    <th>Nombre de programa</th>
    <th>Facultad</th>
    <th>Opciones</th>

    @foreach ($programs as $row)
    <tr>
        <td>{{ $row->cod_program }}</td>
        <td>{{ $row->name_program }}</td>
        <td>{{ $row->cod_faculty }}</td>
        <td>
            <form action="{{ route('programs.delete',$row) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Eliminar</button>
            </form>
            <a href="{{ route('programs.edit',$row) }}" class="btn btn-warning">Editar</a>
        </td>
    </tr>
    @endforeach
</table>

@endsection
