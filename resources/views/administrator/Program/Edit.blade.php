@extends('layouts.app')

@section('content')
<form action="{{ route('programs.update') }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <input type="hidden" name="cod_program" value="{{ $program->cod_program }}">
    <label for="name_program">Nombre de la facultad</label>
    <input type="text" name="name_program" id="name_program" value="{{ $program->name_program }}"><br><br>
    <input type="file" name="image" value="{{ $program->image }}"><br>
    <label for="cod_faculty">Faculty</label>
    <select name="cod_faculty" id="cod_faculty">
        @foreach ($faculty as $row)
        <option value="{{ $row->cod_faculty }}">{{ $row->name_faculty }}</option>
        @endforeach
    </select><br><br>
    <button class="btn btn-primary">Guardar cambios</button>
</form>
@endsection
