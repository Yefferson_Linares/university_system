@extends('layouts.app')

@section('content')
    <form action="{{ route('programs.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <label for="name_program">Nombre del programa</label>
        <input type="text" name="name_program" id="name_program"><br>
        <input type="file" name="image"><br>
        <label for="cod_faculty">Campus</label>
        <select name="cod_faculty" id="cod_faculty">
            @foreach ($faculties as $row)
            <option value="{{ $row->cod_faculty }}">{{ $row->name_faculty }}</option>
            @endforeach
        </select><br>
        <button class="btn btn-primary">Crear</button>
    </form>
@endsection
