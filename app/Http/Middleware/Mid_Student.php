<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class Mid_Student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth()->user();
        if($user->type_user != 1)
        {
            Auth::logout();
            return $request->wantsJson()
            ? new JsonResponse([], 204)
             : redirect('/');
        }
        return $next($request);
    }
}
