<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Redirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth()->user();

        if($user->type_user == 1){
            return redirect()->intended('/estudiante');
        }
        if($user->type_user == 2){
            return redirect()->intended('/administrador');
        }

        return $next($request);
    }
}
