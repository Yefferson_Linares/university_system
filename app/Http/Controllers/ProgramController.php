<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function index()
    {
        $programs = Program::paginate(900);
        return view('administrator.Programs',compact('programs'));
    }

    public function create()
    {
        $faculties = Faculty::paginate(900);
        return view('administrator.program.create',compact('faculties'));
    }

    public function store(Request $request)
    {
        $name = '';
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'\images',$name);
        }
        $programs = new Program();
        $programs->cod_program = 0;
        $programs->name_program = $request->name_program;
        $programs->image = $name;
        $programs->cod_faculty = $request->cod_faculty;
        $programs->save();
        return redirect()->intended('programs');
    }

    public function delete($program)
    {
        $program = Program::find($program);
        $ruta = public_path().'/images/'.$program->image;
        unlink($ruta);
        $program->delete();
        return back();
    }

    public function edit($program)
    {
        $program = Program::find($program);
        $faculty = Faculty::paginate(900);
        return view('administrator.program.edit',compact('program','faculty'));
    }

    public function update(Request $request)
    {
        $name = '';
        $program = Program::find($request->cod_program);
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images',$name);

            $ruta = public_path().'/images/'.$program->image;
            unlink($ruta);
        }
        $program->name_program = $request->name_program;
        $program->image = $name;
        $program->cod_faculty = $request->cod_faculty;
        $program->save();
        return redirect()->intended('programs');
    }
}
