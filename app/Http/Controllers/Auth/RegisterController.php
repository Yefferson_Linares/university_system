<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required','email','string'],
            'identification_card' => ['required','integer'],
            'direction' => ['required','string'],
            'telephone' => ['required','string'],
            'city_​​of_residence' => ['required','string'],
            'city_​​of_origin' => ['required','string'],
            'nationality' => ['required','string'],
            'password' => ['required', 'string', 'min:1', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        settype($data['identification_card'],'int');
          return User::create([
              'identification_card' => $data['identification_card'],
              'name' => $data['name'],
              'email' => $data['email'],
              'direction' => $data['direction'],
              'telephone' => $data['telephone'],
              'city_​​of_residence' => $data['city_​​of_residence'],
              'city_​​of_origin' => $data['city_​​of_origin'],
              'nationality' => $data['nationality'],
              'type_user' => 1,
              'password' => Hash::make($data['password']),
              'cod_faculty' => 1,
            ]);
    }
}
