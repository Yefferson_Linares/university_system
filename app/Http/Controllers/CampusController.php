<?php

namespace App\Http\Controllers;

use App\Campus;
use GuzzleHttp\RetryMiddleware;
use Illuminate\Http\Request;

class CampusController extends Controller
{
    public function index()
    {
        $campuses = Campus::paginate(900);
        return view('administrator.Campus',compact('campuses'));
    }

    public function create()
    {
        return view('administrator.campuses.Create');
    }

    public function store(Request $request)
    {
        $campus = new Campus();
        $campus->cod_campus = 0;
        $campus->name_campus = $request->name_campus;
        $campus->direction = $request->direction;
        $campus->save();
        return redirect()->intended('/campus');
    }

    public function delete($campus)
    {
        $campus = Campus::find($campus);
        $campus->delete();
        return back();
    }

    public function edit($campus)
    {
        $campus = Campus::find($campus);
        return view('administrator.campuses.Edit',compact('campus'));
    }

    public function update(Request $request)
    {
        $campus = Campus::find($request->cod_campus);
        $campus->name_campus = $request->name_campus;
        $campus->direction = $request->direction;
        $campus->save();
        return redirect()->intended('/campus');
    }
}
