<?php

namespace App\Http\Controllers;

use App\Campus;
use App\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function index()
    {
        $faculties = Faculty::paginate(900);
        return view('administrator.Faculties',compact('faculties'));
    }

    public function create()
    {
        $campuses = Campus::paginate(900);
        return view('administrator.Faculties.Create',compact('campuses'));
    }

    public function store(Request $request)
    {
        $faculties = new Faculty();
        $faculties->cod_faculty = 0;
        $faculties->name_faculty = $request->name_faculty;
        $faculties->cod_campus = $request->cod_campus;
        $faculties->save();
        return redirect()->intended('facultades');
    }

    public function delete($faculty)
    {
        $faculty = Faculty::find($faculty);
        $faculty->delete();
        $campuses = Campus::paginate(900);
        return view('administrator.Faculties.Create',compact('campuses'));
    }

    public function edit($faculty)
    {
        $faculty = Faculty::find($faculty);
        $campuses = Campus::paginate(900);
        return view('administrator.Faculties.Edit',compact('faculty','campuses'));
    }

    public function update(Request $request)
    {
        $faculty = Faculty::find($request->cod_faculty);
        $faculty->name_faculty = $request->name_faculty;
        $faculty->cod_campus = $request->cod_campus;
        $faculty->save();
        return redirect()->intended('facultades');
    }
}
